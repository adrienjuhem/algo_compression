//transforme le texte en collection de trigram
function trigram(content) {

	var trigramArray = [];
	var splitContent = content.split("");
	var temp;

	for (var i = 0; i <= splitContent.length-1; i= i + 3) {
		temp = 0;

		if (splitContent[i+2] == undefined) {
			temp = splitContent[i] + splitContent[i+1];
		} else if (splitContent[i+1] == undefined) {
			temp = splitContent[i];
		} else {
			temp = splitContent[i] + splitContent[i+1] + splitContent[i+2];	
		}

		trigramArray.push(temp);
	}
	console.log(trigramArray.length);
	return trigramArray;
}

//construit un alphabet en tableau
function genAlphabeticArray( ) {
	var a = [], i = 'a'.charCodeAt(0), j = 'z'.charCodeAt(0);

	for (; i <= j; ++i) {
		a.push(String.fromCharCode(i));
	}
	i = 'a'.charCodeAt(0); 
	j = 'z'.charCodeAt(0);
	for (; i <= j; ++i) {
		
		a.push(String.fromCharCode(i).toUpperCase());
	}
	
	return a;
}

//enleve les doublons sur le tableau
function trigramOccurence(trigramArray) {

	var trigramNoDouble = [];
	for (var i = 0; i <= trigramArray.length - 1; i++) {

		if(!isExist(trigramNoDouble, trigramArray[i])) {

			trigramNoDouble.push(trigramArray[i]);
		}
	}

	// console.log(trigramNoDouble);
	return trigramNoDouble;

}


//regarde si une valeur existe dans un tableau
function isExist(array , value) {
	//console.log(array);
	var verif = false;
	for (var j = 0; j <= array.length - 1; j++) {

		//console.log(array[j] + ' : ' + value);
		if (array[j] == value) {
			verif = verif + true;

		} 

		verif = verif + false;

	}
	//console.log(verif);
	return verif;
}


//construit un tableua de clés 
function getDictionary(array) {


	var alphabet = genAlphabeticArray();
	var dictionnary = [];
	var temp = [];
	x = 0;
	//console.log(array);
	//console.log(array.length + ' : ' + alphabet.length);

	for (var i = 0; i <= alphabet.length - 1; i++){

		for (var j = 0; j <= alphabet.length - 1; j++) {
			temp = [];
			if (dictionnary.length < array.length) {

				temp.push(alphabet[i] + alphabet[j], array[x]);
				dictionnary.push(temp);
				x++;

			}
		}
	}

	return dictionnary;
}


function compression(array, dictionnary) {

	var compressedArray = [];

	for (var i = 0; i <= array.length - 1; i++) {
		for (var j = 0; j <= dictionnary.length - 1; j++) {

			if(array[i] == dictionnary[j][1]) {
				compressedArray.push(dictionnary[j][0]);
			}

		}	

	}

	return compressedArray;

}

function getCompressedText(compressedArray, dictionnary) {
	var compressedText  = '';
	compressedText = compressedText + dictionnary.length + ' ';
	temp ='';
	console.log(compressedArray);
	console.log(compressedArray.length);
	
	for (var j = 0; j <= dictionnary.length - 1; j++) {
		compressedText = compressedText + dictionnary[j][1];
	}

	for (var i = 0; i <= compressedArray.length - 1; i++) {
	
		temp = temp + compressedArray[i]
	}
	console.log(temp);
	console.log(compressedText);
	compressedText = compressedText + temp;

	console.log(compressedText)
	return compressedText;

}


//transforme le texte en collection de trigram
function getSize(content) {

	var temp = content.split(" ", 1);
	var size = temp[0];

	return size;
}


function getTrigramArray(content, size) {
	var array = content.split("");
	console.log(array.length);
	var first = array.indexOf(" ") + 1;
	var stop = (parseInt(size) + 1) * 3;
	var array2 = array.slice(first, stop);

	var arrayTrigram = [];
	var temp;	

	for (var i = 0; i <= array2.length-1; i= i + 3) {
		temp = 0;

		if (array2[i+2] == undefined) {
			temp = array2[i] + array2[i+1];
		} else if (array2[i+1] == undefined) {
			temp = array2[i];
		} else {
			temp = array2[i] + array2[i+1] + array2[i+2];	
		}

		arrayTrigram.push(temp);
	}
	return arrayTrigram;
}

function getCompressedArray(content, size) {
	var array = content.split("");
	var nb = array.indexOf(" ");
	var trigram = (parseInt(size)) * 3;
	var first = nb + trigram;
	var stop = array.length;
	var array2 = array.slice(first, stop);
	var arrayOfKey = [];
	for (var i = 0; i <= array2.length-1; i= i + 2) {
		temp = 0;

		temp = array2[i] + array2[i+1];
		arrayOfKey.push(temp);
	}

	return arrayOfKey;
}


function decompression(array, dictionnary) {

	var decompressedArray = [];

	for (var i = 0; i <= array.length - 1; i++) {
		for (var j = 0; j <= dictionnary.length - 1; j++) {

			if(array[i] == dictionnary[j][0]) {
				decompressedArray.push(dictionnary[j][1]);
			}

		}	

	}

	return decompressedArray;

}

function getDecompressedText(array) {
	var decompressedText  = '';
	
	for (var i = 0; i <= array.length - 1; i++) {
		decompressedText = decompressedText + array[i];
	}


	return decompressedText;
}


function verif_extension(content)
{
	extension=recuperer_extension(fichier);
	if(extension!=".pdf")
	{
		alert("le fichier doit comporter l'extension .pdf");
	}
	else alert("bon fichier");
}