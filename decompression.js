//console.log('decompression');

var content;
var fileInput = document.querySelector('#file');

fileInput.addEventListener('change', function() {

	var reader = new FileReader();

	reader.addEventListener('load', function() {
		$( ".decompresser" ).click(function() {

			content = reader.result;
			console.log(content);


			var size = getSize(content);
			console.log(size);

			var trigramArray = getTrigramArray(content, size);
			console.log(trigramArray);

			var dictionary = getDictionary(trigramArray);
			console.log(dictionary);

			var compressedArray = getCompressedArray(content, size)
			console.log(compressedArray);

			var decompressedArray = decompression(compressedArray, dictionary)
			console.log(decompressedArray);

			var decompressedText = getDecompressedText(decompressedArray);
			console.log(decompressedText);

			
			var file = new Blob([decompressedText], {type: "text/plain;charset=utf-8"});
			    if (window.navigator.msSaveOrOpenBlob) // IE10+
			    	window.navigator.msSaveOrOpenBlob(file, 'decompressed_file.txt');
			    else { // Others
			    	var a = document.createElement("a"),
			    	url = URL.createObjectURL(file);
			    	a.href = url;
			    	a.download = 'decompressed_file.txt';
			    	document.body.appendChild(a);
			    	a.click();
			    	setTimeout(function() {
			    		document.body.removeChild(a);
			    		window.URL.revokeObjectURL(url);  
			    	}, 0); 
			    }
			});

	});
	reader.readAsText(fileInput.files[0]);

});









